ifeq ($(HOSTNAME),cherry-creek.nscee.edu)
FF=ifort
DEBUG=-traceback
FFLAGS=-O2 -r8 -mcmodel=large -shared-intel -mkl=parallel ${DEBUG}
else
# try generic gfortran
FF=gfortran
FFLAGS = -fno-sign-zero
LIBS= -lblas -llapack
endif
