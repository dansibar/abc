      subroutine milkepot(rij, v)
c
c   $RCSfile: surface.f,v $ $Revision: 1.4 $
c   $Date: 89/08/04 14:22:14 $
c   $State: Stable $
c
c-----------------------------------------------------------------------
c This routine supplies the potential energy surface for the
c reactive scattering codes.
c
c On entering: The array rij contains the internuclear distances
c              in units of bohr (NOT mass scaled).
c
c              The common /pes/ must contain a character string
c              of length 20 that properly describes the potential.
c
c On Exit:     The variable v is the value of the potential energy
c              surface in Hartree atomic units.
c
c
c-----------------------------------------------------------------------
      implicit real*8 (a-h,o-z)
      dimension rij(3)
      dimension vderiv(3)
      DATA CONV1,CONV2,CONV3 / .52917706D-8,27.211608,1.6021892D-12 /

      parameter (ido=0)

c-----------------------------------------------------------------------
c This call to surfaec supplies the potential energy surface
c for the particular system of interest.
c-----------------------------------------------------------------------

c Test for nuclear singularity

	if (rij(1) .GT. 0.2d0 .AND. rij(2) .GT. 0.2d0
     $        .AND. rij(3) .GT. 0.2d0) then

       call milke(rij,v)

c Here we do not need to add the ezero because it is already
c added in the pescci.f subroutine
      v=v*CONV2

      else

c Some Constant High value (80.0 eV)
      v=2.93984d0

      endif

      return
      end
