      subroutine bkmp2pot(rij, v)
c
c   $RCSfile: surface.f,v $ $Revision: 1.4 $
c   $Date: 89/08/04 14:22:14 $
c   $State: Stable $
c
c-----------------------------------------------------------------------
c This routine supplies the potential energy surface for the
c reactive scattering codes.
c
c On entering: The array rij contains the internuclear distances
c              in units of bohr (NOT mass scaled).
c
c On Exit:     The variable v is the value of the potential energy
c              surface in Hartree atomic units.
c
c-----------------------------------------------------------------------
      implicit real*8 (a-h,o-z)
      dimension rij(3)
      dimension vderiv(3)
      DATA CONV1,CONV2,CONV3 / .52917706D-8,27.211608,1.6021892D-12 /

      parameter (ido=0)

c==================================================
c     Ezero as given in surface_data.dat from the output of bkmp2test
c     agrees with the value of  0.174 496 Eh given in the paper
c     A refined H3 potential energy surface - J. Chem. Phys. 104, 7139 (1996)
      ezero=0.17449577081


c     ezero consistant with the value in BK APH code
c     ezero=0.174499242d0
c-----------------------------------------------------------------------
c This call supplies the potential energy surface for the particular
c system of interest.
c-----------------------------------------------------------------------

c Test for nuclear singularity

	if (rij(1) .GT. 0.2d0 .AND. rij(2) .GT. 0.2d0
     $        .AND. rij(3) .GT. 0.2d0) then

      call bkmp2(rij,v,vderiv,ido)

c Shift to bottom of H + D2 asymptotic well
      v=v+ezero

c     hartree-electron volt relationship  27.21138602 eV
      v = v*CONV2
      else

c Some Constant High value (80.0 eV)
      v=2.93984d0

      endif

      return
      end
