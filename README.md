# abc scattering code repository


## modifications to original abc

I have tried to keep all changes to the original source code minimal and optional.

1. Specify potential name - can choose from multiple potentials for a given system
2. Scaling factors for some internal abc parameters - useful for convergence tests. 

## abc python tool

a utility tool to help with various abc tasks such as:
* creating input files and directory structure 
* bulk job submission
* convert the parity adapted smatrix to helicity representation
* generate crossections for H3 - to be generalised.
