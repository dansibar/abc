import numpy as np
from pdb import set_trace
import matplotlib.pyplot as plt
from math import pi, sqrt
from scipy import constants

# data
mu = 0.672*constants.physical_constants[
    'atomic mass unit-electron volt relationship'][0]
speed_of_light = constants.c/constants.angstrom
hbar = constants.physical_constants['Planck constant over 2 pi in eV s'][0]
# set_trace()

data = np.loadtxt('smatrix.csv', skiprows=1)
energies = np.unique(data[:, 8])
for energy in energies:
    prob = 0.0
    for i in data:
        if i[8] == energy and i[7] == 0:
            k2 = 2.0*mu*(i[8] - i[10])/(hbar**2*speed_of_light**2)
            prefac = pi*(2.0*i[9] + 1.0)/k2
            prob += prefac*3.0*(i[11]**2 + i[12]**2)/2
        if i[8] == energy and i[7] == 1:
            k2 = 2.0*mu*(i[8] - i[10])/(hbar**2*speed_of_light**2)
            prefac = pi*(2.0*i[9] + 1.0)/k2
            prob += prefac*3.0*(i[13]**2 + i[14]**2)/2
            prob += prefac*3.0*(i[11]**2 + i[12]**2)/2
    print energy, prob
