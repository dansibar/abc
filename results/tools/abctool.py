#!/usr/bin/env python
"""
Utility script for abc jobs

Usage:
  abctool create-input <prefix>  <Jmax> [--Jmin=<Jmin> --force --qsub]
  abctool gen-smatrix <prefix>  <Jmax> [--Jmin=<Jmin> --query=<query>]
  abctool state-to-state <prefix>
  abctool submit <dirs>... [--dryrun]
  abctool -h | --help
  abctool --version

Options:
  -h --help      Show this screen.
  --version      Show version.
  --force        overwrite existing files if they exist [default: False]
  --qsub         submit jobs to the pbs queue [default: False]
  --dryrun       print command but dont execute [default: False]
  gen-smatrix    Compute the smatrix in the standard helicity representation
  create-input   Generate input file for submission
  state-to-state Generate state-to-state crosssections
  submit         submit jobs to the pbs queue
  --Jmin=<Jmin>  Minimum value of Total J [default: 0]
  <Jmax>         Maximum value of Total J
  --query=<query> A pandas datafram query to filter out unwatned values
"""
from abc_module.docopt import docopt
from abc_module.abc_analysis import AbcAnalysis
from abc_module.pre_processing import AbcPreprocessor
from abc_module.post_processing import AbcPostprocessor
from abc_module.abc_logging import setup_logger
from os import system

logger = setup_logger(__name__)


def sanitize(string):
    """
    Convert docopt args to valid python variable names
    """
    return string.strip('<>-').replace('-', '_').lower()


def submit(dirs, dryrun):
    for dir in dirs:
        cmd = 'qsub {}/abc.pbs'.format(dir)
        if dryrun:
            print cmd
        else:
            system(cmd)


if __name__ == '__main__':
    ARGS = docopt(__doc__)
    for arg in ARGS:
        ARGS[sanitize(arg)] = ARGS.pop(arg)
    logger.info('Started')
    print ARGS
    logger.debug(ARGS)
    if ARGS['create_input']:
        preprocessor = AbcPreprocessor(**ARGS)
        preprocessor.create_input()
    elif ARGS['gen_smatrix']:
        postprocessor = AbcPostprocessor(**ARGS)
        postprocessor.convert_smatrix()
    elif ARGS['state_to_state']:
        abc = AbcAnalysis(**ARGS)
        abc.cross_sections()
    elif ARGS['submit']:
        submit(ARGS['dirs'], ARGS['dryrun'])
    logger.info('Finished')
