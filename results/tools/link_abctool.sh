#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mkdir -pv ~/mypath
echo 'PATH=$PATH:$HOME/mypath; export $PATH ' >> ~/.bashrc 
pushd ~/mypath
ln -s $DIR/abctool.py abctool
popd
source ~/.bashrc
