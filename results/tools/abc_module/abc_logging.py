import logging
from sys import stdout


def setup_logger(name, fname='abc.log', std_out=True):
    logger = logging.getLogger(name)
    logger.setLevel(logging.INFO)

    # create a file handler

    handler = logging.FileHandler(fname)
    handler.setLevel(logging.INFO)

    # create a logging format

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    # add the handlers to the logger

    logger.addHandler(handler)

    # add stdout
    if std_out:
        ch = logging.StreamHandler(stdout)
        ch.setFormatter(formatter)
        logger.addHandler(ch)

    return logger
