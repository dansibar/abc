import pandas as pd
import numpy as np
from abc_logging import setup_logger
from generic import (
    input_params,
    loop_generator,
    valid_combination,
    detect_symmetry,
    kdelta,
    is_evn,
    is_odd,
    abs_sq,
    check_unitarity,
    )
from math import sqrt

logger = setup_logger(__name__)


class AbcPostprocessor():

    def __init__(self, prefix, jmin, jmax, query, **kwargs):
        self.prefix = prefix
        self.file_type = 'csv'
        self.filename = '{}-{{}}.{}'.format(self.prefix, self.file_type)
        self.jmin = jmin
        self.jmax = jmax
        self.query = query
        self.symm = detect_symmetry()
        self.params = input_params(jmin, jmax, self.symm)

    def convert_smatrix(self):
        """
        compute crossections for output files in prefix folders for jtotal j
        in range jrange need to add parity loop in createparams
        """
        self.get_raw_data()
        self.helicity_representation()
        input_filename = self.filename.format('input')
        smatrix_filename = self.filename.format('smatrix')
        logger.info('writing input to file {}'.format(input_filename))
        self.raw_input.to_csv(input_filename)
        logger.info('checking smatrix is unitary')
        check_unitarity(self.smatrix)
        logger.info('writing smatrix to file {}'.format(smatrix_filename))
        self.smatrix.to_csv(smatrix_filename, sep='\t', index=False)

    def helicity_representation(self):
        """
        Convert the parity adapted S-matrix elements from the abc output file
        to standard helicity-representation S-matrix elements
        """
        on = [
            'a', 'tot_nrg', 'k', 'v', 'j', 'jtot',
            "ap", "vp", "kp", "jp", "cha_nrg"]
        smatrix = pd.merge(
            self.raw_smatrix.query('ipar==1'),
            self.raw_smatrix.query('ipar==-1'),
            how='outer', on=on, suffixes=('_+', '_-'))
        smatrix.fillna(0, inplace=True)
        self.smatrix = smatrix
        # there can be some NaNs in smatrix where the smatrix elements are zero
        # for example jtot=0 ipar=0
        smatrix.drop(['ipar_+', 'ipar_-'], axis=1, inplace=True)
        self.compute_helicity_selements()

    def compute_helicity_selements(self):
        """
        compute the helicity s matrix elements from parity adapted s matrix
        elements eqns 1 and 2 of abc paper
        """
        smtrx = self.smatrix
        vkdelta = np.vectorize(kdelta)
        prefac = (1.0+vkdelta(smtrx['k'], 0))*(1.0+vkdelta(smtrx['kp'], 0))
        prefac = np.sqrt(prefac)/2.0
        smtrx['Re(S)_k+'] = prefac*(smtrx['Re(S)_+'] + smtrx['Re(S)_-'])
        smtrx['Im(S)_k+'] = prefac*(smtrx['Im(S)_+'] + smtrx['Im(S)_-'])
        prefac = prefac*(-1)**smtrx['jtot']
        smtrx['Re(S)_k-'] = prefac*(smtrx['Re(S)_+'] - smtrx['Re(S)_-'])
        smtrx['Im(S)_k-'] = prefac*(smtrx['Im(S)_+'] - smtrx['Im(S)_-'])
        smtrx.drop(
            ['Im(S)_+', 'Re(S)_+', 'Re(S)_-', 'Im(S)_-'], axis=1, inplace=True)

    def get_raw_data(self):
        """
        Process the raw abc output files and create a unified smatrix
        and input dataframes
        """
        raw_smatricies = []
        raw_inputs = []
        raw_bases = []
        for combination in loop_generator(
                self.params, valid_combination=valid_combination):
            dirname = "{}_{}".format(
                self.prefix, "_".join(
                    ["".join([str(y) for y in x]) for x in combination]))
            logger.info('extracting data from {}'.format(dirname))
            logger.info('combination({})'.format(combination))
            file_name = "{}/raw-{}.out".format(dirname, 'smatrix')
            raw_smatrix = pd.read_csv(file_name, delim_whitespace=True)
            file_name = "{}/raw-{}.out".format(dirname, 'basis')
            raw_basis = pd.read_csv(file_name, delim_whitespace=True)
            file_name = "{}/raw-{}.out".format(dirname, 'input')
            raw_input = pd.read_csv(
                file_name,
                delim_whitespace=True,
                squeeze=True,
                index_col=0,
                header=None)
            for col in ['ipar', 'jpar', 'jtot']:
                raw_smatrix[col] = raw_input[col]
                raw_input.drop(col, inplace=True)
            for col in ['kmin', 'kmax', 'jout']:
                raw_input.drop(col, inplace=True)

            # drop internal abc channel labelling
            raw_basis.drop('Channel', inplace=True, axis=1)
            raw_smatrix.drop('jpar', inplace=True, axis=1)

            raw_inputs.append(raw_input)
            raw_smatricies.append(raw_smatrix)
            raw_bases.append(raw_basis)

        if not all([x.equals(raw_inputs[0]) for x in raw_inputs]):
            msg = 'Not all folders contain the same input'
            logger.error(msg)
        input = raw_inputs[0]
        smatrix = pd.concat(raw_smatricies)
        basis = pd.concat(raw_bases).drop_duplicates()
        #  add the channel energies to smatrix
        smatrix = pd.merge(smatrix, basis, on=['a', 'v', 'j', 'k'])
        self.raw_input = input
        if self.query:
            before = len(smatrix)
            logger.info('applying query {}'.format(self.query))
            smatrix = smatrix.query(self.query)
            after = len(smatrix)
            logger.info('reduced smatrix from {} to {}'.format(before, after))
        self.raw_smatrix = smatrix
        self.raw_smatrix.to_csv('raw_smatrix.csv', sep='\t', index=False)
