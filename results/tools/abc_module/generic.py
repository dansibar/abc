from itertools import product
from math import sqrt
from numpy import real, conj
import numpy as np
from abc_logging import setup_logger
from os import mkdir
from os.path import isdir

logger = setup_logger(__name__)

ABC_TOL = 0.00000001


def makedir(dirname):
    if not isdir(dirname):
        logger.info('creating directory {}'.format(dirname))
        mkdir(dirname)
    else:
        logger.info('directory {} already exists'.format(dirname))


def sum_k(df):
    """
    Combine the probabilities accounting for k and -k
    """
    vkdelta = np.vectorize(kdelta)
    dkprime = vkdelta(df["kp"], 0)
    dk = vkdelta(df["k"], 0)
    return (
        2-dk*dkprime)*df["P_(n'k'->nk)"] + (2-dk-dkprime)*df["P_(n'-k'->nk)"]


def check_unitarity(smatrix, drop=True):
    """
    Check unitarity
    """
    groupby = ["jtot", "a", "v", "j", "k", "tot_nrg"]
    compute_probabilities(smatrix)
    heading = "Sigma_P"  # sum over probabilities
    vkdelta = np.vectorize(kdelta)
    smatrix[heading] = smatrix["P_(n'k'->nk)"] + (
        1-vkdelta(smatrix["kp"], 0))*smatrix["P_(n'-k'->nk)"]
    grouped_smatrix = smatrix.groupby(groupby).aggregate(np.sum)
    values = grouped_smatrix[heading].values
    if drop:
        smatrix.drop(
            [heading, "P_(n'k'->nk)", "P_(n'-k'->nk)"], inplace=True, axis=1)
    if np.allclose(values, np.ones(len(values)), atol=ABC_TOL):
        logger.info('smatrix is unitary to tolerance:{}'.format(ABC_TOL))
    else:
        logger.error('smatrix NOT unitary to tolerance:{}'.format(ABC_TOL))


def compute_probabilities(smatrix):
    if "P_(n'k'->nk)" not in smatrix:
        smatrix["P_(n'k'->nk)"] = abs_sq(
            smatrix['Re(S)_k+'], smatrix['Im(S)_k+'])
    else:
        logger.info('skipping {} - already computed'.format("P_(n'k'->nk)"))
    if "P_(n'-k'->nk)" not in smatrix:
        smatrix["P_(n'-k'->nk)"] = abs_sq(
            smatrix['Re(S)_k-'], smatrix['Im(S)_k-'])
    else:
        logger.info('skipping {} - already computed'.format("P_(n'-k'->nk)"))


def abs_sq(real, imag):
    """
    vecotorize method to return elementwise |v|^2
    """
    return real**2 + imag**2


def is_evn(x):
    return x % 2 == 0


def is_odd(x):
    return x % 2 == 1


def reduced_mass2(m1, m2):
    """
    2-body reduced mas in whatever units
    """
    return m1*m2/(m1+m2)


def reduced_mass3(m1, m2, m3):
    """
    3-body reduced mas in whatever units
    """
    return sqrt(m1*m2*m3/(m1+m2+m3))


class ABCTOOL_EXCEPTION(Exception):
    pass


def kdelta(i, j):
    """
    Kronecker delta
    """
    if i == j:
        return 1
    else:
        return 0


def vkdelta(i, j):
    return np.vectorize(kdelta)(i, j)


def evn(i):
    """
    vectorized method to return if number is even
    """
    return vkdelta(i % 2, 0)


def odd(i):
    """
    vectorized method to return if number is even
    """
    return vkdelta(i % 2, 1)


def valid_combination(params):
    """
    Are the params parameters legitimate wrt symmetry considerations?
    """
    param_dict = dict(params)
    if param_dict.get('jtot') == 0 and param_dict.get('ipar') == -1:
        logger.info(
            '{} not a valid combination of parameters'.format(param_dict))
        return False
    else:
        return True


def loop_generator(params, valid_combination=None):
    """
    given a dict of keys and iterable values return a generator
    of combinations. Each combination is a list of tuples where
    the first element is the key and the second element is the
    particluar value

    optionally pass a function to filter unwanted values
    """

    if valid_combination is None:
        valid_combination = lambda x: True

    param_names = tuple([key for key in params])
    return (zip(param_names, i) for i in product(*params.values()) if
            valid_combination(zip(param_names, i)))


def input_params(jmin, jmax, symm):
    """
    Convert input symmetry and total j quantum number to dictionary of input
    parameters to loop over
    """
    params = {}
    params['jtot'] = range(int(jmin), int(jmax)+1)
    params['ipar'] = [1, -1]
    if symm == 'abc':
        params['jpar'] = [0]
    elif symm == 'ab2' or symm == 'a3':
        params['jpar'] = [-1, 1]
    else:
        fail_msg = 'unknown symmetry {}'.format(symm)
        raise ABCTOOL_EXCEPTION(fail_msg)
    return params


def read_input(fname='abc.d'):
    """
    update var in file fname to value val
    """
    with open(fname, 'r') as file:
        filedata = file.readlines()

    # drop the first and last items
    filedata.pop(0)
    filedata.pop(-1)
    input = dict([item.strip().split('=') for item in filedata])
    input['mass'] = input.pop('mass').split(',')
    return input


def detect_symmetry():
    input = read_input()
    masses = set(input['mass'])
    if len(masses) == 3:
        symm = 'abc'
    elif len(masses) == 2:
        symm = 'ab2'
    elif len(masses) == 1:
        symm = 'a3'
    else:
        raise ABCTOOL_EXCEPTION('unknown symmetry')
    logger.info('symmetry {} detected'.format(symm))
    return symm
