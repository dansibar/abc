import os
import shutil
from re import sub
from generic import (
    input_params,
    loop_generator,
    valid_combination,
    detect_symmetry)


class AbcPreprocessor():

    def __init__(self, prefix, jmin, jmax, force, qsub, **kwargs):
        """
        Class for ABC pre processing: everything before running the abc code
        creating input files directories submitting jobs etc
        """
        self.jmax = jmax
        self.jmin = jmin
        self.force = force
        self.qsub = qsub
        self.prefix = prefix
        self.symm = detect_symmetry()
        self.params = input_params(jmin, jmax, self.symm)

    def create_input(self):
        """
        create directory strucure with params files
        """
        cp_files = ['abc.pbs', 'abc.d', 'abc.sh']
        for combination in loop_generator(
                self.params, valid_combination=valid_combination):
            dirname = "{}_{}".format(self.prefix,  "_".join(
                ["".join([str(y) for y in x]) for x in combination]))
            if os.path.isdir(dirname) and not self.force:
                print "Folder {} exists".format(dirname)
                print "delete first if you really want to overwrite"
                print "Or run with --force"
            else:
                if os.path.isdir(dirname):
                    print 'Deleting {} force={}'.format(dirname, self.force)
                    shutil.rmtree(dirname)
                print 'creating folder {}'.format(dirname)
                os.mkdir(dirname)
                for file in cp_files:
                    shutil.copy(file, '{}/{}'.format(dirname, file))
                for param in combination:
                    self.modify_input(
                        '{}/abc.d'.format(dirname), param[0], param[1])
                if self.qsub:
                    os.chdir(dirname)
                    print 'submitting job'
                    os.system('qsub abc.pbs')
                    os.chdir('..')

    @staticmethod
    def modify_input(fname, var, val):
        """
        update var in file fname to value val
        """
        filedata = None
        with open(fname, 'r') as file:
            filedata = file.read()

        # Replace the target string
        regex_str = '{}={}'.format(var, '.*')
        replc_str = '{}={}'.format(var, val)
        filedata = sub(regex_str, replc_str, filedata)

        # Write the file out again
        with open(fname, 'w') as file:
            file.write(filedata)
