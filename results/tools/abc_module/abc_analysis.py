import pandas as pd
import numpy as np
from math import pi, sqrt
from scipy import constants as const
from scipy.constants import physical_constants as pconst

from pdb import set_trace
from abc_logging import setup_logger
from abc_module.generic import (
    reduced_mass2, kdelta, check_unitarity, compute_probabilities,
    detect_symmetry, is_evn, is_odd, abs_sq, sum_k,
    makedir, evn, odd,
)

logger = setup_logger(__name__)

AMU2EV = pconst['atomic mass unit-electron volt relationship'][0]
SPEED_OF_LIGHT = const.c/const.angstrom
HBAR = pconst['Planck constant over 2 pi in eV s'][0]
HBARC = HBAR*SPEED_OF_LIGHT

vkdelta = np.vectorize(kdelta)


def sum_k(df):
    """
    Combine the probabilities accounting for k and -k
    """
    dkp = vkdelta(df["kp"], 0)
    dk = vkdelta(df["k"], 0)
    df['P'] = (1+(1-dk)*(1-dkp))*df['Pk+'] + (2-dk-dkp)*df['Pk-']


def evn_evn(rsn, isn, rsr, isr):
    return (rsn+rsr)**2 + (isn+isr)**2


def odd_odd(rsn, isn, rsr, isr):
    return (rsn-rsr)**2 + (isn-isr)**2 + 2.0*(rsr**2+isr**2)


def odd_evn(rsr, isr):
    return rsr**2+isr**2


def evn_odd(rsr, isr):
    return 3.0*odd_evn(rsr, isr)


class AbcAnalysis():

    def __init__(self, prefix, **kwargs):
        self.prefix = prefix
        self.smatrix_file = '{}-smatrix.csv'.format(self.prefix)
        self.input_file = '{}-input.csv'.format(self.prefix)
        self.import_smatrix()
        self.import_input()
        self.mass = self.input['mass1']
        self.mu = reduced_mass2(self.mass*2, self.mass)
        logger.info("reduced mass: {}".format(self.mu))

    def import_smatrix(self):
        """
        Import smatrix as pandas dataframe
        """
        logger.info("importing abc smatrix from file: {}".format(
            self.smatrix_file))
        self.smatrix = pd.read_csv(self.smatrix_file, sep='\t')
        logger.info(
            "finished importing abc smatrix from file: {}".format(
                self.smatrix_file))
        check_unitarity(self.smatrix)

    def import_input(self):
        """
        Import abc input as pandas Series
        """
        logger.info(
            "importing abc input from file: {}".format(self.input_file))
        self.input = pd.read_csv(
            self.input_file, squeeze=True, header=None, index_col=0)

    def symmetrise(self):
        cols = ['Re(S)_k+', 'Re(S)_k-', 'Im(S)_k+', 'Im(S)_k-']
        dap = vkdelta(self.smatrix['ap'], 2)
        for col in cols:
            self.smatrix[col] *= 1 + dap*((1.0/sqrt(2))-1)
        on = [
            "a", "v", "j", "k", "vp", "jp", "kp",
            "tot_nrg", "jtot", "cha_nrg"]
        df = pd.merge(
            self.smatrix.query("ap==1"), self.smatrix.query("ap==2"),
            how='outer', on=on, suffixes=('_n', '_r'),
            left_index=False, right_index=False)

        df.fillna(0.0, inplace=True)  # not sure this fills anything?

        df['Pk+'] = evn(df['j'])*evn(df['jp'])*evn_evn(
            df['Re(S)_k+_n'], df['Im(S)_k+_n'],
            df['Re(S)_k+_r'], df['Im(S)_k+_r'])
        df['Pk+'] += odd(df['j'])*odd(df['jp'])*odd_odd(
            df['Re(S)_k+_n'], df['Im(S)_k+_n'],
            df['Re(S)_k+_r'], df['Im(S)_k+_r'])
        df['Pk+'] += odd(df['j'])*evn(df['jp'])*odd_evn(
            df['Re(S)_k+_r'], df['Im(S)_k+_r'])
        df['Pk+'] += evn(df['j'])*odd(df['jp'])*evn_odd(
            df['Re(S)_k+_r'], df['Im(S)_k+_r'])

        df['Pk-'] = evn(df['j'])*evn(df['jp'])*evn_evn(
            df['Re(S)_k-_n'], df['Im(S)_k-_n'],
            df['Re(S)_k-_r'], df['Im(S)_k-_r'])
        df['Pk-'] += odd(df['j'])*odd(df['jp'])*odd_odd(
            df['Re(S)_k-_n'], df['Im(S)_k-_n'],
            df['Re(S)_k-_r'], df['Im(S)_k-_r'])
        df['Pk-'] += odd(df['j'])*evn(df['jp'])*odd_evn(
            df['Re(S)_k-_r'], df['Im(S)_k-_r'])
        df['Pk-'] += evn(df['j'])*odd(df['jp'])*evn_odd(
            df['Re(S)_k-_r'], df['Im(S)_k-_r'])

        to_drop = ['Re(S)_k+_r', 'Re(S)_k-_r', 'Im(S)_k+_r', 'Im(S)_k-_r',
                   'Re(S)_k+_n', 'Re(S)_k-_n', 'Im(S)_k+_n', 'Im(S)_k-_n',
                   'ap_n', 'ap_r']
        df.drop(to_drop, axis=1, inplace=True)
        df['ap'] = 1
        self.smatrix = df

    def output_cross_sections(self, mode):
        if mode == 'v_resolved':
            logger.info('calculating v resolved cross sections')
            gro = ["a", "v", "ap", "vp", "col_nrg", "tot_nrg"]
        elif mode == 'j_resolved':
            logger.info('calculating j resolved cross sections')
            gro = ["a", "v", "j", "ap", "vp", "jp", "col_nrg", "tot_nrg"]
        elif mode == 'a_resolved':
            logger.info('calculating a resolved cross sections')
            gro = ["a", "ap", "col_nrg", "tot_nrg"]
        df = self.smatrix.groupby(gro).aggregate(np.sum).reset_index()
        gro.pop()
        gro.pop()
        data_dir = '{}-data'.format(self.prefix)
        makedir(data_dir)
        header = [
            'tot_nrg/Ev', 'col_nrg/Ev', 'integral_cross_section/A^2', 'Prob']
        for name, group in df.groupby(gro):
            file_name = "_".join(
                "{}{}".format(i, int(j)) for i, j in zip(gro, name))
            file_name = '{}/{}.csv'.format(data_dir, file_name)
            logger.info('writing cross sections to file {}'.format(file_name))
            group.to_csv(file_name,
                         columns=['tot_nrg', 'col_nrg', 'cross_section', 'P'],
                         index=False, header=header)

    def cross_sections(self):
        if self.mass == 1.008:
            logger.info('H3 detected symmeterizing the wavefunction')
            self.symmetrise()
        else:
            self.smatrix['Pk+'] = (
                self.smatrix['Re(S)_k+']**2 + self.smatrix['Im(S)_k+']**2)
            self.smatrix['Pk-'] = (
                self.smatrix['Re(S)_k-']**2 + self.smatrix['Im(S)_k-']**2)

        sum_k(self.smatrix)
        self.smatrix['col_nrg'] = (
            self.smatrix['tot_nrg'] - self.smatrix['cha_nrg'])
        k_sq = 2.0*self.mu*AMU2EV*self.smatrix['col_nrg']/HBARC**2
        self.smatrix['P'] *= (2.0*self.smatrix['jtot'] + 1)/(
            2.0*self.smatrix['j'] + 1)
        self.smatrix['cross_section'] = self.smatrix['P']*pi*2.0/k_sq
        self.output_cross_sections('j_resolved')
